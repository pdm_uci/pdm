#include <iostream>
#include <string>
#include <cassert>
#include <sys/stat.h>
#include <stdlib.h> 
#include <string.h>
#include <stdexcept>
#include <stdio.h> 

#include "pfm.h"
#include "rbfm.h"
#include "test_util.h"

using namespace std;

int RBFTest_8(RecordBasedFileManager *rbfm) {
    // Functions tested
    // 1. Create Record-Based File
    // 2. Open Record-Based File
    // 3. Insert Record
    // 4. Read Record
    // 5. Close Record-Based File
    // 6. Destroy Record-Based File
    cout << endl << "***** In RBF Test Case 8 *****" << endl;
   
    RC rc;
    string fileName = "test8";

    // Create a file named "test8"
    rc = rbfm->createFile(fileName);
    assert(rc == success && "Creating the file should not fail.");

	rc = createFileShouldSucceed(fileName);
    assert(rc == success && "Creating the file should not fail.");

    // Open the file "test8"
    FileHandle fileHandle;
    rc = rbfm->openFile(fileName, fileHandle);
    assert(rc == success && "Opening the file should not fail.");
      
    RID rid; 
    int recordSize = 0;
    void *record = malloc(100);
    void *returnedData = malloc(100);

    vector<Attribute> recordDescriptor;
    createRecordDescriptor(recordDescriptor);
    
    // Initialize a NULL field indicator
    int nullFieldsIndicatorActualSize = getActualByteForNullsIndicator(recordDescriptor.size());
    unsigned char *nullsIndicator = (unsigned char *) malloc(nullFieldsIndicatorActualSize);
	memset(nullsIndicator, 0, nullFieldsIndicatorActualSize);

    // Insert a record into a file and print the record
//    prepareRecord(recordDescriptor.size(), nullsIndicator, 8, "Anteater", 24, 170.1, 5000, record, &recordSize);
//    cout << endl << "Inserting Data:" << endl;
//    rbfm->printRecord(recordDescriptor, record);
//
//    rc = rbfm->insertRecord(fileHandle, recordDescriptor, record, rid);
//    assert(rc == success && "Inserting a record should not fail.");
//
//    // Given the rid, read the record from file
//    rc = rbfm->readRecord(fileHandle, recordDescriptor, rid, returnedData);
//    assert(rc == success && "Reading a record should not fail.");
//
//    cout << endl << "Returned Data:" << endl;
//    rbfm->printRecord(recordDescriptor, returnedData);
//
    //user defined test for delete record
//    prepareRecord(recordDescriptor.size(), nullsIndicator, 8, "Anteater", 24, 170.1, 5000, record, &recordSize);
//    cout << endl << "Inserting Data:" << endl;
//    rbfm->printRecord(recordDescriptor, record);
//
//    rc=rbfm->insertRecord(fileHandle, recordDescriptor, record, rid);
//    assert(rc==success && "Insert failed");
//    cout<<"Inserted with RID: "<<rid.pageNum<<", "<<rid.slotNum<<endl;
//
//    RID rid2;
//    prepareRecord(recordDescriptor.size(), nullsIndicator, 8, "Peter", 10, 10.5, 3500, record, &recordSize);
//    cout << endl << "Inserting Data:" << endl;
//    rbfm->printRecord(recordDescriptor, record);
//
//    rc=rbfm->insertRecord(fileHandle, recordDescriptor, record, rid2);
//    assert(rc==success && "Insert failed");
//    cout<<"Inserted with RID: "<<rid2.pageNum<<", "<<rid2.slotNum<<endl;
//
//    cout<<"deleting RID: "<<rid.pageNum<<", "<<rid.slotNum<<endl;
//    rc=rbfm->deleteRecord(fileHandle,recordDescriptor,rid);
//    assert(rc== success && "Delete failed");
//
//    cout<<"reading: "<<rid.pageNum<<", "<<rid.slotNum<<endl;
//    rc=rbfm->readRecord(fileHandle,recordDescriptor,rid,returnedData);
//    if(rc==0){
//    	cout<<"Error. Trying to read deleted record"<<endl;
//    	rbfm->printRecord(recordDescriptor,returnedData);
//    }
//    else if(rc==-1){
//    	cout<<"OK. deleted record"<<endl;
//    }
//    rc=rbfm->deleteRecord(fileHandle,recordDescriptor,rid);
//    if(rc==0){
//    	cout<<"error deleting a deleted record"<<endl;
//    }
//    else if(rc==-1){
//    	cout<<"good. cannot delete an already  deleted one"<<endl;
//    }
//
//    rc=rbfm->readRecord(fileHandle,recordDescriptor,rid2,returnedData);
//    cout<<"Returned data2:"<<endl;
//    rbfm->printRecord(recordDescriptor,returnedData);
//
    //user defined test ends for delete record

    //user defined test for update record

	prepareRecord(recordDescriptor.size(), nullsIndicator, 3, "Tom", 24, 70.1, 5000, record, &recordSize);
	cout << endl << "Inserting Data:" << endl;
	rbfm->printRecord(recordDescriptor, record);

	rc=rbfm->insertRecord(fileHandle, recordDescriptor, record, rid);
	assert(rc==success && "Insert failed");
	cout<<"\nInserted with RID: "<<rid.pageNum<<", "<<rid.slotNum<<endl;

	prepareRecord(recordDescriptor.size(), nullsIndicator, 5, "Hardy", 10, 159.6, 8900, record, &recordSize);
	cout << endl << "Inserting Data:" << endl;
	rbfm->printRecord(recordDescriptor, record);

	RID rid2;
	rc=rbfm->insertRecord(fileHandle, recordDescriptor, record, rid2);
	assert(rc==success && "Insert failed");
	cout<<"\nInserted with RID: "<<rid2.pageNum<<", "<<rid2.slotNum<<endl;

	prepareRecord(recordDescriptor.size(), nullsIndicator, 6, "Comedy", 16, 359.6, 89001, record, &recordSize);
	cout << endl << "Inserting Data:" << endl;
	rbfm->printRecord(recordDescriptor, record);

	RID rid3;
	rc=rbfm->insertRecord(fileHandle, recordDescriptor, record, rid3);
	assert(rc==success && "Insert failed");
	cout<<"\nInserted with RID: "<<rid3.pageNum<<", "<<rid3.slotNum<<endl;


	RBFM_ScanIterator iterator;
	string conditionAttribute="EmpName";
	CompOp compOp=EQ_OP;
	void* value=malloc(100);
	int len=6;
	memcpy((char*)value,&len,sizeof(int));
	char *str="Tom";
	memcpy((char*)value+sizeof(int),str,sizeof(str));
	vector<string> attributeNames;
//	attributeNames.push_back("Height");
	attributeNames.push_back("EmpName");
	attributeNames.push_back("Age");
//	attributeNames.push_back("Height");
	vector<Attribute> recordDescriptor2;
	createRecordDescriptor2(recordDescriptor2);

	rbfm->scan(fileHandle,recordDescriptor,conditionAttribute,compOp,value,attributeNames,iterator);
	void *recordScan=malloc(PAGE_SIZE);
	cout<<"1$$$"<<endl;
	while(iterator.getNextRecord(rid,recordScan)!=RBFM_EOF){
		cout<<"run"<<endl;
		rbfm->printRecord(recordDescriptor2,recordScan);
	}
	cout<<"2$$$"<<endl;
	free(recordScan);
//	prepareRecord(recordDescriptor.size(), nullsIndicator, 3, "Ant", 25, 180.1, 8000, record, &recordSize);
//	rc=rbfm->updateRecord(fileHandle,recordDescriptor,record,rid);
//	if(rc==0) cout<<"updated\n";
//	else cout<<"update ERROR\n";
//
//	cout<<"After update\n";
//	rc=rbfm->readRecord(fileHandle,recordDescriptor,rid,returnedData);
//	cout<<"Returned data1: "<<endl;
//	rbfm->printRecord(recordDescriptor,returnedData);
//
//	rc=rbfm->readRecord(fileHandle,recordDescriptor,rid2,returnedData);
//	cout<<"\nReturned data2: "<<endl;
//	rbfm->printRecord(recordDescriptor,returnedData);
//
//	prepareRecord(recordDescriptor.size(), nullsIndicator, 10, "PeterPeter", 10, 5.9, 1007, record, &recordSize);
//	rc=rbfm->updateRecord(fileHandle,recordDescriptor,record,rid2);
//
//	rc=rbfm->readRecord(fileHandle,recordDescriptor,rid2,returnedData);
//	cout<<"\nReturned data: "<<endl;
//	rbfm->printRecord(recordDescriptor,returnedData);
//
//	cout<<"testing equality"<<endl;
//	prepareRecord(recordDescriptor.size(), nullsIndicator, 3, "Ant", 25, 180.1, 8000, record, &recordSize);
//	rc=rbfm->updateRecord(fileHandle,recordDescriptor,record,rid);
//	if(rc==0) cout<<"updated\n";
//	else cout<<"update ERROR\n";
//
//	cout<<"After update\n";
//	rc=rbfm->readRecord(fileHandle,recordDescriptor,rid,returnedData);
//	cout<<"Returned data1: "<<endl;
//	rbfm->printRecord(recordDescriptor,returnedData);

	//user defined test for update record ends

    // Compare whether the two memory blocks are the same
//    if(memcmp(record, returnedData, recordSize) != 0)
//    {
//        cout << "[FAIL] Test Case 8 Failed!" << endl << endl;
//        free(record);
//        free(returnedData);
//        return -1;
//    }
//	cout<<"testing read attr"<<endl;
//
//	string attrname="EmpName";
//	rbfm->readAttribute(fileHandle,recordDescriptor,rid,attrname,record);
//	cout<<"attribute read"<<endl;
//    rbfm->printAttribute(recordDescriptor,attrname,record);
//
//    cout << endl;

    // Close the file "test8"
    rc = rbfm->closeFile(fileHandle);
    assert(rc == success && "Closing the file should not fail.");

    // Destroy the file
    rc = rbfm->destroyFile(fileName);
    assert(rc == success && "Destroying the file should not fail.");

	rc = destroyFileShouldSucceed(fileName);
    assert(rc == success  && "Destroying the file should not fail.");
    
    free(record);
    free(returnedData);

    cout << "RBF Test Case 8 Finished! The result will be examined." << endl << endl;
    
    return 0;
}

int main()
{
    // To test the functionality of the record-based file manager 
    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance(); 
     
    remove("test8");
       
    RC rcmain = RBFTest_8(rbfm);
    return rcmain;
}
